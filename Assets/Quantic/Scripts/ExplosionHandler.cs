﻿using UnityEngine;
using System.Collections;

public class ExplosionHandler : MonoBehaviour {

	public float autoDestroyDelay = 0.35f;

	IEnumerator AutoDestroy() {
		yield return new WaitForSeconds(autoDestroyDelay);
		Destroy(gameObject);
	}

	// Use this for initialization
	void Start () {
		StartCoroutine("AutoDestroy");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
