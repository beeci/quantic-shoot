﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class QUITexture : MonoBehaviour {
	
	public Texture texture;
	public bool onTop = false;
	public bool isBackground = false;
	public bool stretchToEdges = false;

	float fadeTime;
	float fadeStartTime;
	bool fadingOut = false;
	bool fadingIn = false;
	float maxAlpha = 1.0f;

	Rect position;

	public float alpha;

	void OnGUI() {

		Color color = GUI.color;
		color.a = alpha;
		GUI.color = color;

		if (onTop) {
			GUI.depth = 0;
		}
		
		if (isBackground) {
			GUI.depth = 10;
		}
		
		if (stretchToEdges) {
			position.x = 0;
			position.y = 0;
			position.width = Screen.width;
			position.height = Screen.height;
		}
		if (texture != null) {
			GUI.DrawTexture(position, texture);
		}

		color.a = 1.0f;
		GUI.color = color;
	}

	public void FadeIn(float seconds = 3.0f, float destinationAlpha = 1.0f) {
		fadeTime = seconds;
		fadeStartTime = Time.time;
		fadingOut = false;
		fadingIn = true;
		maxAlpha = destinationAlpha;
	}
	
	void FadeOut(float seconds = 3.0f, float fromAlpha = 1.0f) {
		fadeTime = seconds;
		fadeStartTime = Time.time;
		fadingIn = false;
		fadingOut = true;
		maxAlpha = fromAlpha;
	}
	
	public void Update() {
		float currentSpentTime = Time.time - fadeStartTime;
		if (fadingIn) {
			alpha = Mathf.Lerp(0.0F, maxAlpha, currentSpentTime / fadeTime);
		}
		else if (fadingOut) {
			alpha = Mathf.Lerp(maxAlpha, 0.0F, currentSpentTime / fadeTime);
		}
		
		if (currentSpentTime >= fadeTime) {
			if (fadingIn) {
				fadingIn = false;
				FadeOut(fadeTime);
			}
			else {
				fadingOut = false;	
			}
		}
	}
}
