﻿using UnityEngine;
using System.Collections;

public class PausableObject : MonoBehaviour {

	Vector3 originalVelocity = Vector3.zero;
	bool paused = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.Paused && !paused) {
			originalVelocity = rigidbody.velocity;
			rigidbody.velocity = Vector3.zero;
			paused = true;
		}
		else if (!GameManager.Paused && paused){
			rigidbody.velocity = originalVelocity;
			paused = false;
		}
	}
}
