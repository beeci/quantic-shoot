﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProjectilesHandler : MonoBehaviour {

	List<GameObject> projectiles = null;
	public int maxProjectiles = 20;
	public GameObject projectilePrefab;

	static ProjectilesHandler instance;

	public static ProjectilesHandler Instance {
		get {
			return instance;
		}
	}

	void Awake () {
		instance = this;
	}
	
	// Use this for initialization
	void Start () {
		projectiles = new List<GameObject>();
		for (int i = 0; i < maxProjectiles; i++) {
			GameObject projectile = Instantiate(projectilePrefab) as GameObject;
			projectile.transform.position = new Vector3(0, -10, 0);
			projectile.transform.parent = gameObject.transform;
			projectile.SetActive(false);
			projectiles.Add(projectile);
		}
	}
	
	public GameObject newProjectile(Vector3 position, bool bigOne) {

		foreach (GameObject projectile in projectiles) {
			if (projectile.activeSelf == false) {
				projectile.SetActive(true);
				projectile.transform.position = position;
				projectile.rigidbody.velocity = Vector3.zero;
				if (bigOne) {
					projectile.AddComponent<BigProjectile>();
				}
				else {
					projectile.AddComponent<SmallProjectile>();
				}
				return projectile;
			}
		}
		Debug.LogError("No more projectiles to activate");
		return null;
	}

	public void DisableProjectile(GameObject projectile) {
		projectile.transform.position = new Vector3(0, -10, 0);
		Destroy(projectile.GetComponent<ProjectileHandler>());
		projectile.SetActive(false);
	}

	public void Reset() {

		foreach (GameObject projectile in projectiles) {
			projectile.SetActive(false);
		}
	}

	void OnApplicationQuit() {
		foreach (GameObject projectile in projectiles) {
			Destroy(projectile);
		}
		projectiles.Clear();
	}

	// Update is called once per frame
	void Update () {
	}
}
