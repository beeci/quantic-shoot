﻿using UnityEngine;
using System.Collections;

public class EnemyGenerator : MonoBehaviour {

	public GameObject enemyPrefab;
	public GameObject healePrefab;

	public float minimumSpeed = 80;
	public float maximumSpeed = 150;

	public float healerAddSpeed = 100;

	public float minimumDelay = 0.3f;
	public float maximumDelay = 1.0f;

	float adjustedMinimumDelay = 0.0f;
	float adjustedMaximumDelay = 1.0f;

	public float genSideDistance = 50.0f;

	public float increaseDifficultyInterval = 5.0f;

	const float genConstant = 15.0f; // left/right world point distance of the generated enemy

	IEnumerator increaseDifficulty() {
		yield return new WaitForSeconds(increaseDifficultyInterval);

		if (adjustedMinimumDelay > 0.5) {
			adjustedMinimumDelay -= 0.1f;
		}
		if (adjustedMaximumDelay > 0.7) {
			adjustedMaximumDelay -= 0.1f;
		}

		StartCoroutine(increaseDifficulty());
	}

	IEnumerator DropWithDelay(float seconds) {
		
		yield return new WaitForSeconds(seconds);

		if (!GameManager.Paused) {
			Vector3 enemyPos = new Vector3(0.0f, 0.0f, 0.0f);
			enemyPos.x = Random.Range(-genConstant, genConstant);
			enemyPos.y = 7; // set enemy off-screen on y
			Vector3 screenPos = Camera.main.WorldToScreenPoint(enemyPos); // multi-resolution support

			while (screenPos.x <= genSideDistance || screenPos.x >= Screen.width - genSideDistance) {
				enemyPos.x = Random.Range(-genConstant, genConstant);
				screenPos = Camera.main.WorldToScreenPoint(enemyPos);
			}

			if (Random.Range(0, 100) % 13 == 0) {
				GameObject healer = Instantiate(healePrefab) as GameObject;
				healer.transform.position = enemyPos;
				healer.rigidbody.AddForce(new Vector3(0, Random.Range(-minimumSpeed - healerAddSpeed, -maximumSpeed - healerAddSpeed)));
			}
			else {
				GameObject enemy = Instantiate(enemyPrefab) as GameObject;
				enemy.transform.position = enemyPos;

				float enemySize = Random.Range(0.7f, 1); // hitpoint count determined by size

				enemy.transform.localScale = new Vector3(enemySize, enemySize, enemySize);
				enemy.transform.parent = transform;

				EnemyHandler enemyHandler = enemy.GetComponent<EnemyHandler>();
				enemyHandler.Speed = Random.Range(-minimumSpeed, -maximumSpeed);
			}

			StartCoroutine(DropWithDelay(Random.Range(adjustedMinimumDelay, adjustedMaximumDelay)));
		}
	}

	public void Reset() {

		foreach (Transform child in transform) {
			Destroy(child.gameObject);
		}

		GameObject[] healers = GameObject.FindGameObjectsWithTag("Healer");
		foreach (GameObject go in healers) {
			Destroy(go);
		}

		GameObject[] explosions = GameObject.FindGameObjectsWithTag("Explosion");
		foreach (GameObject go in explosions) {
			Destroy(go);
		}

		adjustedMaximumDelay = maximumDelay;
		adjustedMinimumDelay = minimumDelay;
	}
	
	// Use this for initialization
	void Start () {

		adjustedMaximumDelay = maximumDelay;
		adjustedMinimumDelay = minimumDelay;
	}

	public void StartGenerating() {
		StartCoroutine(DropWithDelay(Random.Range(minimumDelay, maximumDelay)));
		StartCoroutine(increaseDifficulty());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
