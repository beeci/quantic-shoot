﻿using UnityEngine;
using System.Collections;

public class PlayerHandler : MonoBehaviour {

	public GameObject projectilePrefab;

	ProjectilesHandler projectilesHandler;

	GUIManager guiManager;

	public float projectileSpeed = 200.0f;

	public float playerMoveSpeedFactor = 0.7f;

	public int maxHitPoints = 3;

	public float sideDistanceCheck = 10;

	int hitPoints = 3;
	bool paused = false;

	AudioSource hitSource;
	public AudioClip hitClip;

	AudioSource shootSource;
	public AudioClip shootClip;

	AudioSource healSource;
	public AudioClip healClip;

	public int HitPoints {
		get { return hitPoints; }
	}

	public void Hit() {
		hitPoints--;
		if (guiManager) {
			guiManager.DisplayHit();
			hitSource.Play();
		}
	}

	public void Reset() {
		transform.position = new Vector3(0, transform.position.y, 0);
		hitPoints = maxHitPoints;
	}

	void Start() {
		hitPoints = maxHitPoints;

		GameObject GUIObject = GameObject.Find("GUI") as GameObject;

		if (GUIObject) {
			guiManager = (GUIObject).GetComponent<GUIManager>();
		}
		hitSource = gameObject.AddComponent<AudioSource>();
		hitSource.clip = hitClip;
		hitSource.loop = false;
		hitSource.playOnAwake = false;
		hitSource.volume = 0.6f;	// hardcoded value, we could output this to editor

		shootSource = gameObject.AddComponent<AudioSource>();
		shootSource.clip = shootClip;
		shootSource.loop = false;
		shootSource.playOnAwake = false;
		shootSource.volume = 0.2f;	// hardcoded value, we could output this to editor

		healSource = gameObject.AddComponent<AudioSource>();
		healSource.clip = healClip;
		healSource.loop = false;
		healSource.playOnAwake = false;
		healSource.volume = 1.0f;	// hardcoded value, we could output this to editor

		projectilesHandler = ProjectilesHandler.Instance;
	}

	// Update is called once per frame
	void Update () {

		if (GameManager.Paused || paused) {
			paused = GameManager.Paused;
			return;
		}

		Vector3 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
		float horizontal;
		float fireValue;
		bool firePressed = InputHandler.Instance.IsKeyDown(InputHandler.keys.Fire, out fireValue);

		if (InputHandler.Instance.IsKeyDown(InputHandler.keys.Horizontal, out horizontal)) {
			float factor = 1.0f;
			if (firePressed) {
				factor = 0.8f;
			}
			if ((horizontal > 0 && screenPosition.x < Screen.width - sideDistanceCheck) ||
			    (horizontal < 0 && screenPosition.x > sideDistanceCheck)) {
				rigidbody.AddForce(new Vector3(horizontal * playerMoveSpeedFactor * factor, 0.0f, 0.0f));
			}
			else {
				rigidbody.velocity = Vector3.zero;
			}
		}

		if (screenPosition.x <= sideDistanceCheck || screenPosition.x > Screen.width - sideDistanceCheck) {
			rigidbody.velocity = Vector3.zero;
		}

		if (firePressed) {
			GameObject projectile = projectilesHandler.newProjectile(transform.position, Random.Range(0, 100) % 13 == 0);

			if (projectile) {
				projectile.rigidbody.AddForce(new Vector3(0.0f, projectileSpeed, 0.0f));
				shootSource.Play();
			}
		}
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "Enemy") {
			Hit();
		}
		if (collision.gameObject.tag == "Healer") {
			hitPoints += 1;
			healSource.Play();
		}
	}

}
