﻿Shader "Custom/HeatDistort" {
	Properties {
		_MainTex ("Color (RGB) Alpha (A)", 2D) = "white"
	}
	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 200
		
		GrabPass {}
		
		Pass {
			CGPROGRAM
// Upgrade NOTE: excluded shader from DX11 and Xbox360; has structs without semantics (struct v2f members screenPos)
#pragma exclude_renderers d3d11 xbox360
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			struct v2f {
				float4 pos : SV_POSITION;
				float4 uv0 : TEXCOORD0;
				float4 screenPos;
			};
			
			sampler2D _GrabTexture;
			
			v2f vert(appdata_base i) {
				v2f output;
				output.pos = mul(UNITY_MATRIX_MVP, i.vertex);
				output.uv0 = i.texcoord;
				output.screenPos = output.pos;
				return output;
			}
			
			half4 frag(v2f i) : COLOR {
				float2 normalized;
				normalized.x = i.screenPos.x * 3.14159;
				normalized.y = (i.screenPos.y) * 3.14159;
				float2 samplePos;
				samplePos.x = sin(_Time * normalized.x);
				samplePos.y = cos(normalized.y);
				
				half4 c = tex2D (_GrabTexture, samplePos);
				
				return c;
			}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}