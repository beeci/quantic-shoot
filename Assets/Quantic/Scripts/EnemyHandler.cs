﻿using UnityEngine;
using System.Collections;

public class EnemyHandler : MonoBehaviour {

	public GameObject explosionPrefab;
	public float speedMultiplier = 0.1f;
	public Color hitColor;
	public Color normalColor;

	float colorChangeTimePassed = 1.0f;

	int value;
	bool killed = false;

	private float _speed;

	public float Speed {
		set { _speed = value; }
	}

	// Use this for initialization
	void Start () {
		value = Mathf.RoundToInt((1.0f - transform.localScale.x) * 10);
	}

	IEnumerator AutoDestroy() {
		yield return new WaitForSeconds(0.4f);
		Destroy(gameObject);
	}

	
	// Update is called once per frame
	void Update () {

		if (GameManager.Paused) {
			return;
		}
		if (colorChangeTimePassed > 0.05 && renderer.material.color != normalColor) {
			renderer.material.color = normalColor;
		}
		else {
			colorChangeTimePassed += Time.deltaTime;
		}

		if (transform.localScale.x < 0.6 && !killed) {
			GameObject explosion = Instantiate(explosionPrefab) as GameObject;
			explosion.transform.position = transform.position;
			gameObject.audio.Play();
			GameManager.KilledEnemy(value);
			gameObject.renderer.enabled = false;
			StartCoroutine(AutoDestroy());
			killed = true;
		}
		else {
			float time = Time.deltaTime * speedMultiplier;
			Vector3 position = transform.position;
			position.y += time * _speed;
			transform.position = position;
		}
	}

	void OnTriggerEnter(Collider collider) {

		if (collider.gameObject.tag == "Projectile") {
			Vector3 scale = transform.localScale;
			float damage = collider.gameObject.GetComponent<ProjectileHandler>().GetDamage() / 10.0f;;
			scale.x -= damage;
			scale.y -= damage;
			scale.z -= damage;
			transform.localScale = scale;
			ProjectilesHandler.Instance.DisableProjectile(collider.gameObject);

			renderer.material.color = hitColor;
			colorChangeTimePassed = 0.0f;
		}
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.name == "BottomCheck") {
			Destroy(gameObject);
		}

		if (collision.gameObject.name == "Player") {
			Destroy(gameObject);
		}
	}
}
