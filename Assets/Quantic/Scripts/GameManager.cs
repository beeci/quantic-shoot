﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;

public class GameManager : MonoBehaviour {


	static PlayerHandler playerHandler;
	static EnemyGenerator enemyGenerator;
	static ProjectilesHandler projectilesHandler;

	static int enemiesKilled = 0;
	public static int EnemiesKilled {
		get { return enemiesKilled; }
	}

	static int score = 0;
	public static int Score {
		get { return score; }
	}

	static bool paused = true;
	public static bool Paused {
		get {
			return paused;
		}
		set {
			if (!value){
				enemyGenerator.StartGenerating();
			}
			paused = value;
		}
	}

	[MethodImpl(MethodImplOptions.Synchronized)]
	public static void KilledEnemy(int value) {
		enemiesKilled += 1;
		score += value * 100;
	}

	public static bool EndGame {
		get {

			if (playerHandler) {
				return (playerHandler.HitPoints == 0);
			}
			else {
				return true;
			}
		}
	}

	public static void OnLoggedIn() {                                                                                          
		Debug.Log("Logged in. ID: " + FB.UserId);
	}
	
	private void OnFBInitComplete() {
		Debug.Log("FB init complete");
		if (FB.IsLoggedIn) {
			Debug.Log("Already logged in");
			OnLoggedIn();
		}
	}

	void Awake() {
		FB.Init(OnFBInitComplete);

		Unibiller.onBillerReady += (state) => {
			if (UnibillState.SUCCESS == state) {
				Debug.Log("Login sucess");
			}
		};

		Unibiller.Initialise();

		Adjust.trackEvent("jeqibb");
		Debug.Log("-------------- jegibb SENT -----------------");

		//FlurryAnalytics.Instance().LogEvent("started");
	}

	// Use this for initialization
	void Start () {
		enemiesKilled = 0;
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		playerHandler = player.GetComponent<PlayerHandler>();
		
		GameObject enemyGen = GameObject.Find("EnemyGenerator");
		enemyGenerator = enemyGen.GetComponent<EnemyGenerator>();

		projectilesHandler = ProjectilesHandler.Instance;	
	}

	IEnumerator Reset() {
		Unibiller.CreditBalance("gold", GameManager.Score);

		yield return new WaitForSeconds(3.0f);
		enemiesKilled = 0;
		score = 0;

		playerHandler.Reset();
		Paused = false;
	}
	
	// Update is called once per frame
	void Update () {
		// poll for game state
		if (playerHandler.HitPoints <= 0 && !Paused) {

			Paused = true;
			audio.Play();

			enemyGenerator.Reset();
			projectilesHandler.Reset();

			StartCoroutine(Reset());
		}
	}
}
