﻿using UnityEngine;
using System.Collections;

public class SmallProjectile : ProjectileHandler {

	public override float GetDamage() {
		return 1;
	}
}
