/**
 * FlurryAnalytics.cs
 * 
 * A Singleton class encapsulating public access methods of Flurry Analytics.
 * 
 * Please read the code comments carefully, or visit 
 * http://www.neatplug.com/integration-guide-unity3d-flurry-analytics-plugin to find information how 
 * to use this program.
 * 
 * End User License Agreement: http://www.neatplug.com/eula
 * 
 * (c) Copyright 2012 NeatPlug.com All rights reserved.
 * 
 * @version 1.3.6
 * @sdk_version(android) 3.2.1
 * @sdk_version(ios) 4.2.3
 *
 */

using UnityEngine;
using System;
using System.Collections;

public class FlurryAnalytics  {
	
	#region Fields		
	
	private class FlurryAnalyticsNativeHelper : IFlurryAnalyticsNativeHelper {
		
#if UNITY_ANDROID	
		private AndroidJavaObject _plugin = null;
#endif		
		public FlurryAnalyticsNativeHelper()
		{
			
		}
		
		public void CreateInstance(string className, string instanceMethod)
		{	
#if UNITY_ANDROID			
			AndroidJavaClass jClass = new AndroidJavaClass(className);
			_plugin = jClass.CallStatic<AndroidJavaObject>(instanceMethod);	
#endif			
		}
		
		public void Call(string methodName, params object[] args)
		{
#if UNITY_ANDROID			
			_plugin.Call(methodName, args);	
#endif
		}
		
		public void Call(string methodName, string signature, object arg)
		{
#if UNITY_ANDROID			
			var method = AndroidJNI.GetMethodID(_plugin.GetRawClass(), methodName, signature);			
			AndroidJNI.CallObjectMethod(_plugin.GetRawObject(), method, AndroidJNIHelper.CreateJNIArgArray(new object[] {arg}));
#endif			
		}
		
		public ReturnType Call<ReturnType> (string methodName, params object[] args)
		{
#if UNITY_ANDROID			
			return _plugin.Call<ReturnType>(methodName, args);
#else
			return default(ReturnType);			
#endif			
		}
	
	};	
	
	private static FlurryAnalytics _instance = null;
	
	#endregion
	
	#region Functions
	
	/**
	 * Constructor.
	 */
	private FlurryAnalytics()
	{	
#if UNITY_ANDROID		
		FlurryAnalyticsAndroid.Instance().SetNativeHelper(new FlurryAnalyticsNativeHelper());
#endif	
#if UNITY_IPHONE
		FlurryAnalyticsIOS.Instance();
#endif
	}
	
	/**
	 * Instance method.
	 */
	public static FlurryAnalytics Instance()
	{		
		if (_instance == null) 
		{
			_instance = new FlurryAnalytics();
		}
		
		return _instance;
	}	
	
	/**
	 * Initialization.
	 * 
	 * @param apiKey
	 *            string - Your App API key.	
	 * 
	 * @param logUseHttps
	 *            bool - True for using HTTPS for log data transferring, false for using HTTP.
	 * 
	 * @param enableCrashReporting
	 *            bool - True for enabling App crash reporting, false for not.
	 * 
	 * @param enableDebugging
	 *            bool - True for printing debug info in the log, false for not.
	 * 
	 */
	public void Init(string apiKey, bool logUseHttps, bool enableCrashReporting, bool enableDebugging)
	{
#if UNITY_ANDROID		
		FlurryAnalyticsAndroid.Instance().Init(apiKey, logUseHttps, enableCrashReporting, enableDebugging);		
#endif	
#if UNITY_IPHONE
		FlurryAnalyticsIOS.Instance().Init(apiKey, logUseHttps, enableCrashReporting, enableDebugging);	
#endif		
	}
	
	/**
	 * Log an event.
	 * 
	 * Use logEvent to count the number of times certain events happen during a session 
	 * of your application. This can be useful for measuring how often users perform 
	 * various actions, for example.
	 * 
	 * @param eventId
	 *          string - The eventId for identifying the event.	
	 * 	
	 */
	public void LogEvent(string eventId)
	{		
#if UNITY_ANDROID	
		FlurryAnalyticsAndroid.Instance().LogEvent(eventId, new string[]{}, false);
#endif
#if UNITY_IPHONE
		FlurryAnalyticsIOS.Instance().LogEvent(eventId, new string[]{}, false);
#endif		
	}	
	
	/**
	 * Log a timed event.
	 * 
	 * Use logEvent to count the number of times certain events happen during a session 
	 * of your application. This can be useful for measuring how often users perform 
	 * various actions, for example.
	 * 
	 * You will need to call EndTimedEvent() to end this timed event.
	 * 
	 * @param eventId
	 *          string - The eventId for identifying the event.	
	 * 	
	 */
	public void LogTimedEvent(string eventId)
	{		
#if UNITY_ANDROID	
		FlurryAnalyticsAndroid.Instance().LogEvent(eventId, new string[]{}, true);
#endif
#if UNITY_IPHONE
		FlurryAnalyticsIOS.Instance().LogEvent(eventId, new string[]{}, true);
#endif		
	}	
	
	/**
	 * Log an event.
	 * 
	 * Use logEvent to count the number of times certain events happen during a session 
	 * of your application. This can be useful for measuring how often users perform 
	 * various actions, for example.
	 * 
	 * @param eventId
	 *          string - The eventId for identifying the event.	
	 * 
	 * @param keyVals
	 *          string[] - Additional info in Key-Value pairs, for instance: 
	 *                     new string[]{"key1:val1","key2:val2"}
	 * 
	 * @param timed
	 *          bool - Whether the event is timed.
	 *                 NOTE: If you log a timed event, you will need to
	 *                 call "EndTimedEvent()" to end the timed event when
	 *                 it ends.
	 */
	public void LogEvent(string eventId, string[] keyVals, bool timed)
	{		
#if UNITY_ANDROID	
		FlurryAnalyticsAndroid.Instance().LogEvent(eventId, keyVals, timed);
#endif
#if UNITY_IPHONE
		FlurryAnalyticsIOS.Instance().LogEvent(eventId, keyVals, timed);
#endif		
	}
	
	/**
	 * End a timed event.
	 * 
	 * @param eventId
	 *          string - The eventId for identifying the event,
	 *                   this should be set up at Flurry dev site.
	 */
	public void EndTimedEvent(string eventId)
	{		
#if UNITY_ANDROID	
		FlurryAnalyticsAndroid.Instance().EndTimedEvent(eventId);
#endif
#if UNITY_IPHONE
		FlurryAnalyticsIOS.Instance().EndTimedEvent(eventId);
#endif
	}		
	
	/**
	 * Log an application error.
	 * 
	 * Use this to log exceptions and/or errors that occur in your app. 	
	 * 
	 * @param errorId
	 *          string - The errorId for identifying the error.                   
	 * 
	 * @param message
	 *          string - The error message.
	 * 
	 * @param errorClass
	 *          string - The error class in which the error occurred.
	 */
	public void LogError(string errorId, string message, string errorClass)
	{		
#if UNITY_ANDROID	
		FlurryAnalyticsAndroid.Instance().LogError(errorId, message, errorClass);
#endif
#if UNITY_IPHONE
		FlurryAnalyticsIOS.Instance().LogError(errorId, message, errorClass);
#endif		
	}
	
	/**
	 * Log a page view.
	 * 
	 * Use this to report page view count. You should call this method
	 * whenever a new page is shown to the user to increment the total count. 
	 * Page view is tracked separately from events.	
	 */
	public void LogPageview()
	{		
#if UNITY_ANDROID	
		FlurryAnalyticsAndroid.Instance().LogPageview();
#endif
#if UNITY_IPHONE
		FlurryAnalyticsIOS.Instance().LogPageview();
#endif		
	}
	
	
	/**
	 * Set the user ID.
	 * 
	 * Use this to log the user's assigned ID or username in your system.
	 * 
	 * @param userId
	 *          string - The userId assigned by your system.                   
	 * 	
	 */
	public void SetUserId(string userId)
	{		
#if UNITY_ANDROID	
		FlurryAnalyticsAndroid.Instance().SetUserId(userId);
#endif
#if UNITY_IPHONE
		FlurryAnalyticsIOS.Instance().SetUserId(userId);
#endif		
	}
	
	/**
	 * Set the user's age.
	 * 
	 * Use this to log the user's age after identifying the user.
	 * 
	 * @param userAge
	 *          int - The user's age obtained from your app.                   
	 * 	
	 */
	public void SetUserAge(int userAge)
	{		
#if UNITY_ANDROID	
		FlurryAnalyticsAndroid.Instance().SetUserAge(userAge);
#endif
#if UNITY_IPHONE
		FlurryAnalyticsIOS.Instance().SetUserAge(userAge);
#endif		
	}
	
	/**
	 * Set the user's gender.
	 * 
	 * Use this to log the user's gender after identifying the user.
	 * 
	 * @param isFemale
	 *          bool - true for female, false for male.                   
	 * 	
	 */
	public void SetUserGender(bool isFemale)
	{		
#if UNITY_ANDROID	
		FlurryAnalyticsAndroid.Instance().SetUserGender(isFemale);
#endif
#if UNITY_IPHONE
		FlurryAnalyticsIOS.Instance().SetUserGender(isFemale);
#endif		
	}	
	
	/**
	 * Set the user's geo location.
	 * 
	 * Use this to log the user's location. You may need extra plugin for
	 * getting the user's geo location, it is recommended that you use
	 * the NativeToolbox plugin from http://www.neatplug.com, it's under
	 * the "Native" section. 
	 * The function can be called periodically to get the plugin updated with
	 * the changed user location.	 
	 * 
	 * @param latitude
	 *          double - The latitude of the location.
	 *
	 * @param longitude
	 *          double - The longitude of the location.
	 * 	
	 */
	public void SetUserLocation(double latitude, double longitude)
	{		
#if UNITY_ANDROID	
		FlurryAnalyticsAndroid.Instance().SetUserLocation(latitude, longitude);
#endif
#if UNITY_IPHONE
		FlurryAnalyticsIOS.Instance().SetUserLocation(latitude, longitude);
#endif		
	}
	
	
	#endregion
}
