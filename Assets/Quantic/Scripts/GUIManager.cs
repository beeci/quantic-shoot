﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook;
using Facebook.MiniJSON;

public class GUIManager : MonoBehaviour {

	// objects for polling
	public GameObject player;

	// scripts for polling
	PlayerHandler playerHandler;

	QUITexture hitTexture;
	Texture photo = null;

	public Font displayFont;

	int sizeMultiplier = 1;
	bool loggedInCalled = false;

	void Awake() {
		playerHandler = player.GetComponent<PlayerHandler>();
		hitTexture = gameObject.GetComponent<QUITexture>();
		hitTexture.alpha = 0.0f;
	}

	public void DisplayHit() {
		hitTexture.FadeIn(0.1f);
	}

	void FBAPICallback(FBResult result) {
		Debug.Log("FBAPICallback called.");
		if (result.Error != null) {
			Debug.Log(result.Error);
		}
	}

	void OnGUI() {

		Font origFont = GUI.skin.font;
		GUI.skin.font = displayFont;

#if UNITY_ANDROID
		GUI.skin.label.fontSize = 30;
		GUI.skin.button.fontSize = 30;
#else
		GUI.skin.label.fontSize = 14;
		GUI.skin.button.fontSize = 14;
#endif

		GUI.Label(new Rect(10 * sizeMultiplier, 10 * sizeMultiplier, 200 * sizeMultiplier, 30 * sizeMultiplier), "Hit Points: " + playerHandler.HitPoints);
		GUI.Label(new Rect(10 * sizeMultiplier, 30 * sizeMultiplier, 300 * sizeMultiplier, 30 * sizeMultiplier), "Total Enemies Shot: " + GameManager.EnemiesKilled);
		GUI.Label(new Rect(10 * sizeMultiplier, 50 * sizeMultiplier, 300 * sizeMultiplier, 30 * sizeMultiplier), "Score: " + GameManager.Score);
		GUI.Label(new Rect(10 * sizeMultiplier, 70 * sizeMultiplier, 300 * sizeMultiplier, 30 * sizeMultiplier), "Gold: " + Unibiller.GetCurrencyBalance("gold"));

		if (GameManager.EndGame) {

			int origFontSize = GUI.skin.label.fontSize;

#if UNITY_ANDROID
			GUI.skin.label.fontSize = 50;
#else
			GUI.skin.label.fontSize = 30;
#endif
			float screenVCenter = Screen.height / 2;
			float screenHCenter = Screen.width / 2;
			GUI.Label(new Rect(screenHCenter - 100 * sizeMultiplier, screenVCenter + 30 * sizeMultiplier, 300 * sizeMultiplier, 50 * sizeMultiplier), "Score: " + GameManager.Score);

#if UNITY_ANDROID
			if (GameManager.Score > 0 && FB.IsLoggedIn) {
				if (GUI.Button(new Rect(screenHCenter - 85 * sizeMultiplier, screenVCenter + 20 * sizeMultiplier, 170 * sizeMultiplier, 30 * sizeMultiplier), "Post score!")) {
					Debug.Log("Sending score...");
					var scoreData = new Dictionary<string, string>() {{"score", GameManager.Score.ToString()}};
					FB.API("/me/scores", Facebook.HttpMethod.POST, FBAPICallback, scoreData);
				}
			}
#endif

			GUI.skin.label.fontSize = origFontSize;
		}
		else if (GameManager.Paused) {
			float screenVCenter = Screen.height / 2;
			float screenHCenter = Screen.width / 2;
#if !UNITY_ANDROID
			GUI.Label(new Rect(screenHCenter - 55 * sizeMultiplier, screenVCenter - 110 * sizeMultiplier, 120 * sizeMultiplier, 30 * sizeMultiplier), "Instructions");
			GUI.Label(new Rect(screenHCenter - 200 * sizeMultiplier, screenVCenter - 90 * sizeMultiplier, 460 * sizeMultiplier, 30 * sizeMultiplier), "Default Movement: Left/Right arrows or A/D");
			GUI.Label(new Rect(screenHCenter - 125 * sizeMultiplier, screenVCenter - 70 * sizeMultiplier, 250 * sizeMultiplier, 30 * sizeMultiplier), "Default Shoot: Space or left mouse button");
#endif
			bool pressed = GUI.Button(new Rect(screenHCenter - 50 * sizeMultiplier, screenVCenter - 10 * sizeMultiplier, 100 * sizeMultiplier, 20 * sizeMultiplier), "Start");
			
			if (pressed) {
				GameManager.Paused = false;

				Adjust.trackEvent("jfg7rt");
				Debug.Log("----- jfg7rt ----- SENT!");
			}
#if UNITY_ANDROID
			if (!FB.IsLoggedIn) {                                                                                                                           
				if (GUI.Button(new Rect(screenHCenter - 85 * sizeMultiplier, screenVCenter + 20 * sizeMultiplier, 170 * sizeMultiplier, 30 * sizeMultiplier), "Login to Facebook")) {                                                                                                            
					FB.Login("email,publish_actions", LoginCallback);                                                        
				}                                                                                                            
			}
			else if (!loggedInCalled) {
				LoginCallback(null);
			}
#endif
		}
		if (!GameManager.Paused) {
			if (GUI.Button(new Rect(Screen.width - 120 * sizeMultiplier, 10 * sizeMultiplier, 100 * sizeMultiplier, 20 * sizeMultiplier), "Pause")) {
				GameManager.Paused = !GameManager.Paused;
			}
		}

#if UNITY_ANDROID
		if (photo) {
			GUI.DrawTexture(new Rect(Screen.width - 55 * sizeMultiplier, 50 * sizeMultiplier, 30 * sizeMultiplier, 30 * sizeMultiplier), photo);
		}

		/*if (GUI.Button(new Rect(Screen.width - 170 * sizeMultiplier, 120 * sizeMultiplier, 150 * sizeMultiplier, 20 * sizeMultiplier), "Purchase FF Fish")) {
			Unibiller.initiatePurchase(Unibiller.AllPurchasableItems[0]);
			Debug.Log("Purchasing: " + Unibiller.AllPurchasableItems[0].name);
		}*/
#endif
		GUI.skin.font = origFont;
	}

	void MyPictureCallback(Texture texture)                                                                                        
	{                                                                                                                              
		if (texture == null)                                                                                                  
		{                                                                                                                          
			// Let's just try again
			LoadPicture(GetPictureURL("me", 128, 128),MyPictureCallback);                               
			return;                                                                                                                
		}                                                                                                                          
		photo = texture;                                                                             
	}
	
	public static string GetPictureURL(string facebookID, int? width = null, int? height = null, string type = null) {
		string url = string.Format("/{0}/picture", facebookID);
		string query = width != null ? "&width=" + width.ToString() : "";
		query += height != null ? "&height=" + height.ToString() : "";
		query += type != null ? "&type=" + type : "";
		query += "&redirect=false";
		if (query != "") url += ("?g" + query);
		return url;
	}

	#region Facebook utils
	void LoginCallback(FBResult result){                                                                                          
	 	Debug.Log("Logged in!");                                                          
		
		if (FB.IsLoggedIn)                                                                     
		{                                                                                      
			GameManager.OnLoggedIn();
			// Reqest player info and profile picture                                                                           
			FB.API("/me?fields=id,first_name,friends.limit(100).fields(first_name,id)", Facebook.HttpMethod.GET, FBAPICallback);  
			LoadPicture(GetPictureURL("me", 128, 128), MyPictureCallback);    
		}
		loggedInCalled = true;
	}
	public string DeserializePictureURLString(string response) {
		object pictureObj = Json.Deserialize(response);
		var picture = (Dictionary<string, object>)(((Dictionary<string, object>)pictureObj)["data"]);
		object urlH = null;
		if (picture.TryGetValue("url", out urlH)) {
			return (string)urlH;
		}
		
		return null;
	}

	delegate void LoadPictureCallback (Texture texture);
	
	IEnumerator LoadPictureEnumerator(string url, LoadPictureCallback callback)
	{
		WWW www = new WWW(url);
		yield return www;
		callback(www.texture);
	}
	void LoadPicture (string url, LoadPictureCallback callback)
	{
		FB.API(url,Facebook.HttpMethod.GET,result => {
			if (result.Error != null){
				Debug.LogError(result.Error);
				return;
			}
			
			var imageUrl = DeserializePictureURLString(result.Text);
			StartCoroutine(LoadPictureEnumerator(imageUrl,callback));
		});
	}
#endregion
	
	// Use this for initialization
	void Start () {
		#if UNITY_ANDROID
		sizeMultiplier = 2;
		#else
		sizeMultiplier = 1;
#endif
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
