﻿namespace com.adjust.sdk {
	public class Util {
		public enum LogLevel {
			Verbose = 1,
			Debug,
			Info,
			Warn,
			Error,
			Assert
		}

		public enum Environment {
			Sandbox,
			Production
		}
	}
}