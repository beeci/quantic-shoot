﻿using UnityEngine;
using System.Collections.Generic;

public class InputHandler : MonoBehaviour {

	public enum keys : byte {
		Horizontal = 0,
		Fire,
	}
	
	private class KeyInfo {
		public float startTime = 0.0f; // in seconds
		public float timout = 0.05f; // in seconds
		public float currentTime = 0.0f; // in seconds;
		public bool pressed = false;
		public float value;

		public KeyInfo(float check_timeout = 0.05f) {
			timout = check_timeout;
			value = 0;
		}
	}

	private static Dictionary<keys, KeyInfo> inputStore;
	private static InputHandler instance;
	private static GameObject ihGameObject;

	public static InputHandler Instance {
		get {
			if(instance == null) {
				ihGameObject = new GameObject("Input Manager");
				instance = ihGameObject.AddComponent<InputHandler>();
				instance.LoadSettings();
				DontDestroyOnLoad(instance);
			}
			return instance;
		}
	}

	void LoadSettings() {
		inputStore = new Dictionary<keys, KeyInfo>();

		inputStore.Add(keys.Horizontal, new KeyInfo(0));
		inputStore.Add(keys.Fire, new KeyInfo());
	}

	public bool IsKeyDown(keys action) {
		return inputStore[action].pressed;
	}

	public bool IsKeyDown(keys action, out float value) {

		value = 0;

		if (inputStore[action].pressed) {
			value = inputStore[action].value;
		}
		return inputStore[action].pressed;
	}

	// Update is called once per frame
	void Update () {

		float deltaTime = Time.deltaTime;
		float currentTime = Time.time;

		Dictionary<keys, KeyInfo>.ValueCollection infos = inputStore.Values;
		
		foreach (KeyInfo info in infos) {
			if (info.currentTime > 0.0f) {
				if (info.currentTime < info.startTime + info.timout) {
					info.currentTime += deltaTime;
					info.pressed = false;
				}
				else {
					info.currentTime = 0.0f;
				}
			}

		}

#if (UNITY_IPHONE || UNITY_ANDROID)
		
		if (Input.touches.Length > 0) {
			KeyInfo info = inputStore[keys.Horizontal];
			if (info.currentTime == 0.0f) {
				info.pressed = true;
				info.value = Input.touches[0].deltaPosition.x / 3; // nice magic
				info.startTime = currentTime;
				info.currentTime = currentTime;
			}

			if (Input.touches.Length == 2 && Input.touches[1].phase != TouchPhase.Canceled) {
				info = inputStore[keys.Fire];
				if (info.currentTime == 0.0f) {
					info.pressed = true;
					info.value = 1f;
					info.startTime = currentTime;
					info.currentTime = currentTime;
				}
			}
		}
		else {
			KeyInfo info = inputStore[keys.Horizontal];
			info.currentTime = 0.0f;
			info.pressed = false;
			info.value = 0.0f;
		}

#else
		float value;

		if ((value = Input.GetAxis("Horizontal")) != 0) {
			KeyInfo info = inputStore[keys.Horizontal];
			if (info.currentTime == 0.0f) {
				info.pressed = true;
				info.value = value;
				info.startTime = currentTime;
				info.currentTime = currentTime;
			}
		}
		else {
			KeyInfo info = inputStore[keys.Horizontal];
			info.currentTime = 0.0f;
			info.pressed = false;
			info.value = 0.0f;
		}

		if (Input.GetAxis("Fire") != 0) {
			KeyInfo info = inputStore[keys.Fire];
			if (info.currentTime == 0.0f) {
				info.pressed = true;
				info.value = value;
				info.startTime = currentTime;
				info.currentTime = currentTime;
			}
		}
#endif
	}
}
