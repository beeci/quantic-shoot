﻿using UnityEngine;
using System.Collections;

public abstract class ProjectileHandler : MonoBehaviour {

	public abstract float GetDamage();

	// Use this for initialization
	void Start () {
		transform.localScale = new Vector3(GetDamage() / 10.0f, GetDamage() / 10.0f, GetDamage() / 10.0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
