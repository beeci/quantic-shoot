﻿using UnityEngine;
using System.Collections;

public class BottomCheckManager : MonoBehaviour {

	public GameObject player;
	PlayerHandler playerHandler;
	// Use this for initialization
	void Start () {
		playerHandler = player.GetComponent<PlayerHandler>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "Enemy") {
			playerHandler.Hit();
		}
	}
}
