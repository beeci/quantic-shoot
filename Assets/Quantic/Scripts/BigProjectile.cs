﻿using UnityEngine;
using System.Collections;

public class BigProjectile : ProjectileHandler {

	public override float GetDamage() {
		return 2;
	}
}
