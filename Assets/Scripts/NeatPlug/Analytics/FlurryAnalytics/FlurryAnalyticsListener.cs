/**
 * FlurryAnalyticsListener.cs
 * 
 * FlurryAnalyticsListener listens to the Flurry Analytics events.
 * File location: Assets/Scripts/NeatPlug/Analytics/FlurryAnalytics/FlurryAnalyticsListener.cs
 * 
 * Please read the code comments carefully, or visit 
 * http://www.neatplug.com/integration-guide-unity3d-flurry-analytics-plugin to find information 
 * about how to integrate and use this program.
 * 
 * End User License Agreement: http://www.neatplug.com/eula
 * 
 * (c) Copyright 2012 NeatPlug.com All Rights Reserved.
 * 
 * @version 1.3.6
 * @sdk_version(android) 3.2.1
 * @sdk_version(ios) 4.2.3
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlurryAnalyticsListener : MonoBehaviour {
	
	private static bool _instanceFound = false;
	
	void Awake()
	{		
		if (_instanceFound)
		{
			Destroy(gameObject);
			return;
		}
		_instanceFound = true;
		// The gameObject will retain...
		DontDestroyOnLoad(this);
		FlurryAnalytics.Instance();
	}
}
