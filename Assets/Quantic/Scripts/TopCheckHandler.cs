﻿using UnityEngine;
using System.Collections;

public class TopCheckHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter(Collider collider) {
		if (collider.gameObject.tag == "Projectile") {
			ProjectilesHandler.Instance.DisableProjectile(collider.gameObject);
		}
	}
}
